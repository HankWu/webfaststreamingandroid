package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.myapplication.encode.AvcEncoder;
import com.viatech.sample.webservice.ServerService;
import com.viatech.sample.webservice.SocketServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.ByteBuffer;


public class MainActivity extends AppCompatActivity {
    final static String TAG = "WFS";
    SurfaceView surfaceView;
    AvcEncoder avcEncoder = null;
    Camera2 camera2 = null;
    Helper helper = null;
    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "WebServer resumes.");
        Intent intent = new Intent(this, ServerService.class);
        startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, ServerService.class);
        stopService(intent);
        Log.e(TAG, "WebServer pauses.");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        helper = new Helper();

        helper.setupAutoHideSystemUI(this);
        helper.forceScanAllCameras(this);


        surfaceView = findViewById(R.id.cameraview);


        SocketServer.setInstanceCallback(new SocketServer.InstanceCallback() {
            @Override
            public void onCreated() {

                SocketServer socketServer = SocketServer.getInstance();
                socketServer.setCallback(new SocketServer.Callback() {
                    @Override
                    public void onStartStreaming() {
                        Log.d(TAG, "onStartStreaming: ");
                        camera2 = new Camera2(getApplicationContext(), "Camera2://index:0|width:1280|height:720");
                        camera2.setOutputSurface(surfaceView.getHolder().getSurface());
                        camera2.setOtherSurface(getEncoderSurface());
                        encoderStart();
                        camera2.start();
                    }

                    @Override
                    public void onStopStreaming() {
                        Log.d(TAG, "onStopStreaming: ");

                    }


                });
            }
        });
    }




    public void encoderStart() {
        avcEncoder.start();
    }

    public void encoderStop() {
        avcEncoder.close();
    }

    byte[] concate(byte[] a, byte[] b) {
        byte[] c = new byte[a.length+b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    byte[] spspps = null;
    boolean bFirst = false;
    boolean sendFirst = false;
    public Surface getEncoderSurface() {
        try {
            AvcEncoder.EncodeParameters parameters = new AvcEncoder.EncodeParameters(1280,720,2*1024*1024, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            avcEncoder = new AvcEncoder(parameters, new AvcEncoder.EncodedFrameListener() {
                @Override
                public void onFirstSpsPpsEncoded(byte[] sps, byte[] pps) {
                    Log.d(TAG, "onFirstSpsPpsEncoded: ");
                    spspps = concate(sps,pps);
                    spspps = concate(sps, spspps);

                    bFirst = true;
                }

                @Override
                public boolean onFrameEncoded(ByteBuffer nalu, MediaCodec.BufferInfo info) {
                    if(bFirst) {
                        Log.d(TAG, "onFrameEncoded: ");
                        byte[] b = new byte[nalu.remaining()];
                        nalu.get(b, 0, b.length);

                        SocketServer.getInstance().getMutex().lock();
                        byte[] bbb = concate(spspps, b);
                        Log.d("TTTTTS", "sendBuffer: " + bbb.length);
                        SocketServer.getInstance().getQueue().offer(ByteBuffer.wrap(bbb));

                        SocketServer.getInstance().getMutex().unlock();
                    }

                    return false;
                }
            });
            return avcEncoder.getInputSurface();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
