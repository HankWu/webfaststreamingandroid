package com.example.myapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceView;

import com.example.myapplication.encode.VIARecorder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * Created by HankWu on 2017/11/10.
 */
@TargetApi(21)
public class Camera2 {
    Context mContext = null;
    ImageReader mImageReader;
    CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    CameraCaptureSession mCaptureSession;
    CaptureRequest mPreviewRequest;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    SurfaceTexture mSurfaceTexture = null;
    boolean bImageReaderEnable = false;
    Surface mSurface = null;
    CameraManager manager;
    String cameraId = "";
    final static String TAG = "Camera2";
    boolean bRecord = false;
    VIARecorder mVIARecorder = null;
    int mWidth = 0;
    int mHeight = 0;
    Surface mEncodeSurface = null;
    SurfaceView mDisplaySurfaceView = null;
    SurfaceTexture mDisplaySurfaceTexture = null;

    int mIndex = -1;
    float mScalingFactor = 1;

    int rawWidth = 0;
    int rawHeight = 0;
    int rawNumber = 5;

    public void setRawSize(int w,int h, int n) {
        rawHeight = h;
        rawWidth = w;
        rawNumber = n;
    }



    public void close() {
        if(mVIARecorder!=null) {
            mVIARecorder.stop();
            mVIARecorder = null;
        }
        bRecord = false;
        release();
    }


    int mRecordBitrate = 0;
    int mRecordFPS = 0;
    int mRecordPerodicTime = 0;
    String mRecordPath = "";
    VIARecorder.FileListener mRecordFileListener = null;

    public void enableRecord(String path, int bitrate, int fps, int  perodicTimeInSec, VIARecorder.FileListener fileListener) {
        bRecord = true;
        mRecordPath = path;
        mRecordBitrate = bitrate;
        mRecordFPS = fps;
        mRecordPerodicTime = perodicTimeInSec;
        mRecordFileListener = fileListener;
    }

    public Camera2(Context c, String url) {


        String querys = url.toLowerCase().replace("camera2://","");
        String[] parameters  = querys.split("\\|");
        Log.d(TAG,querys+","+parameters.length);

        for(int i=0;i<parameters.length;i++) {
            Log.d(TAG,parameters[0]);
            String[] param = parameters[i].split(":");
            if(param[0].toLowerCase().equals("index")) {
                mIndex = Integer.parseInt(param[1]);
                Log.d(TAG, "Camera2: index:"+mIndex);
            } else if (param[0].toLowerCase().equals("width")) {
                mWidth = Integer.parseInt(param[1]);
                Log.d(TAG, "Camera2: width:"+mWidth);
            } else if (param[0].toLowerCase().equals("height")) {
                mHeight = Integer.parseInt(param[1]);
                Log.d(TAG, "Camera2: height:"+mHeight);
            } else if (param[0].toLowerCase().equals("")) {
                mScalingFactor = Float.parseFloat(param[1]);
                Log.d(TAG, "Camer2: scaling factor:"+mScalingFactor);
            }
        }

        cameraId = mIndex+"";


        sensorRect = getSensorSize(c,cameraId);
        startBackgroundThread();
        manager = (CameraManager) c.getSystemService(Context.CAMERA_SERVICE);
        mContext = c;

        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5f);

    }
//
//    private Camera2(Context c, int id, int width, int height , SurfaceView surfaceView, FrameListener listener) {
//        manager = (CameraManager) c.getSystemService(Context.CAMERA_SERVICE);
//        mContext = c;
//        startBackgroundThread();
//        cameraId = id + "";
//        mWidth = width;
//        mHeight = height;
//        mDisplaySurfaceView = surfaceView;
//        mFrameListener = listener;
//        if(mFrameListener!=null) bImageReaderEnable = true;
//    }
//
//    private Camera2(Context c, int id, int width, int height , SurfaceTexture surfaceTexture, FrameListener listener) {
//        manager = (CameraManager) c.getSystemService(Context.CAMERA_SERVICE);
//        mContext = c;
//        startBackgroundThread();
//        cameraId = id + "";
//        mWidth = width;
//        mHeight = height;
//        mDisplaySurfaceTexture = surfaceTexture;
//        mFrameListener = listener;
//    }

    public void setOutputSurface(Surface surface) {
        mSurface = surface;
    }

    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened( CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected( CameraDevice cameraDevice) {
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError( CameraDevice cameraDevice, int error) {
            cameraDevice.close();
            mCameraDevice = null;
        }
    };


    public static Rect getSensorSize(Context context, String cameraId) {
        Rect activeArraySizeRect = null;
        try {
            CameraManager mCameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics mCameraCharacteristics
                    = mCameraManager.getCameraCharacteristics(cameraId);
            activeArraySizeRect = mCameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);

        } catch (CameraAccessException e) {
            e.printStackTrace();
            return null;
        }
        return activeArraySizeRect;
    }

    public void stop() {
        close();
    }

    @TargetApi(21)
    public void start() {

        try {
            manager.openCamera(cameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    Surface otherSurface;

    public void setOtherSurface(Surface s) {
        otherSurface = s;
    }
    Rect sensorRect = null;
    public static RectF mResult = null;
    public Paint mPaint = null;

    @TargetApi(26)
    private void createCameraPreviewSession() {
        try {
            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            List<Surface> surfaceList = new ArrayList<>();
            CameraManager cameraManager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
//            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
//            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 0);
//
//            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(mIndex+"");
//            Range<Integer> fps[] = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
//            for(Range<Integer> f : fps) {
//                Log.d(TAG, "AAAAAA: "+f.getLower()+"~"+f.getUpper());
//            }
//
            Range<Integer> ff = new Range<>(30,30);
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE,ff);

            if(mSurface!=null) {
                mPreviewRequestBuilder.addTarget(mSurface);
                surfaceList.add(mSurface);
            }

            if(otherSurface!=null) {
                mPreviewRequestBuilder.addTarget(otherSurface);
                surfaceList.add(otherSurface);
            }


//
//            if(bRecord) {
//                mVIARecorder = new VIARecorder(mRecordPath, mWidth, mHeight, mRecordBitrate, mRecordFPS, mRecordPerodicTime, VIARecorder.Mode.Surface);
//                mVIARecorder.setFileListener(mRecordFileListener);
//                mEncodeSurface = mVIARecorder.getInputSurface();
//                surfaceList.add(mEncodeSurface);
//                mPreviewRequestBuilder.addTarget(mEncodeSurface);
//                mVIARecorder.start();
//            }

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(surfaceList,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured( CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }
                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {

                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest, null, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                 CameraCaptureSession cameraCaptureSession) {
//                            showToast("Failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.setPriority(Thread.MAX_PRIORITY);
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }



    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void releaseCamera() {
        if (null != mCaptureSession) {
            mCaptureSession.close();
            mCaptureSession = null;
        }
        if (null != mCameraDevice) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
        if (null != mImageReader) {
            mImageReader.close();
            mImageReader = null;
        }
    }

    public void release() {
        if(mBackgroundThread!=null) stopBackgroundThread();
        releaseCamera();
    }


    /**
     * Comparator based on area of the given {@link Size} objects.
     */
    @TargetApi(21)
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }
}
