var ws = null;
var downloadFile = "";
var serverClosed = false;
var fileStart = false;
var fileEnd = true;
var MyBlobBuilder = function() {
    this.parts = [];
};

MyBlobBuilder.prototype.append = function(part) {
    this.parts.push(part);
    this.blob = undefined;
};

MyBlobBuilder.prototype.getBlob = function() {
    if (!this.blob) {
        this.blob = new Blob(this.parts, {
            type: "text/plain"
        });
    }
    return this.blob;
};
var myBlobBuilder = undefined;

// variables to save last mouse position
var startX;
var startY;

// this var will hold the index of the hit-selected text
var selectedText = -1;

var anchors = [];

function draw() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // draw anchor
    ctx.fillStyle = "#7CFC00";
    for (let i = 0; i < anchors.length; ++i) {
        let anchor = anchors[i];
        ctx.fillText(anchor.text, anchor.x, anchor.y);
    }

    // draw shape
    drawShape();
}

// test if x,y is inside the bounding box of texts[textIndex]
function anchorHitTest(x, y, testIndex) {
    let anchor = anchors[testIndex];
    return (x >= anchor.x && x <= anchor.x + anchor.width && y >= anchor.y - anchor.height && y <= anchor.y);
}

function mouseDownHandler(e) {

    e.preventDefault();

    let canvas = document.getElementById("canvas");
    let rect = canvas.getBoundingClientRect();
    startX = parseInt(e.clientX - rect.left);
    startY = parseInt(e.clientY - rect.top);

    // Put your mousedown stuff here
    for (let i = 0; i < anchors.length; ++i) {
        if (anchorHitTest(startX, startY, i)) {
            selectedText = i;
        }
    }
}

function mouseUpHandler(e) {
    e.preventDefault();
    selectedText = -1;
}

function mouseOutHandler(e) {
    e.preventDefault();
    selectedText = -1;
}

function mouseMoveHandler(e) {
    let canvas = document.getElementById('canvas');

    var rect = canvas.getBoundingClientRect();
    mouseX = parseInt(e.clientX - rect.left);
    mouseY = parseInt(e.clientY - rect.top);


    if (selectedText < 0) {
        return;
    }
    e.preventDefault();

    // Put your mousemove stuff here
    var dx = mouseX - startX;
    var dy = mouseY - startY;
    startX = mouseX;
    startY = mouseY;

    var anchor = anchors[selectedText];
    anchor.x += dx;
    anchor.y += dy;
    draw();
}

function drawShape() {
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext("2d");
    let rect = canvas.getBoundingClientRect();

    // draw the shape from anchors
    ctx.beginPath();
    ctx.moveTo( anchors[0].x + anchors[0].width, anchors[0].y);
    ctx.lineTo( anchors[1].x, anchors[1].y);
    ctx.lineTo( anchors[2].x, anchors[2].y);
    ctx.lineTo( anchors[3].x + anchors[3].width, anchors[3].y);
    ctx.closePath();
    ctx.fillStyle = "rgba(39, 58, 93, 0.7)";
    ctx.fill();
}

function updateCanvas() {
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext("2d");
    let rect = canvas.getBoundingClientRect();

    // initialize anchor position
    let topLeftX = 0.25*rect.width;
    let topLeftY = 0.25*rect.height;
    let topRightX = 0.75*rect.width;
    let topRightY = 0.25*rect.height;
    let bottomLeftX = 0.25*rect.width;
    let bottomLeftY = 0.75*rect.height;
    let bottomRightX = 0.75*rect.width;
    let bottomRightY = 0.75*rect.height;

    // set up anchor text
    anchors = [
        { text: "Anchor[Top-Left]", x: topLeftX, y: topLeftY, align: "end" },
        { text: "Anchor[Top-Right]", x: topRightX, y: topRightY, align: "start" },
        { text: "Anchor[Bottom-Right]", x: bottomRightX, y: bottomRightY, align: "start" },
        { text: "Anchor[Bottom-Left]", x: bottomLeftX, y: bottomLeftY, align: "end" },
    ];

    // calc the size of this text for hit-testing purposes
    ctx.font = "16px verdana";
    ctx.fillStyle = "#7CFC00";        // line color

    for(let i = 0; i < anchors.length; ++i) {
        anchors[i].width = ctx.measureText(anchors[i].text).width;
        anchors[i].height = 30;

        if(anchors[i].align === "end") {
            anchors[i].x = anchors[i].x - anchors[i].width;
        }
        ctx.fillText(anchors[i].text, anchors[i].x, anchors[i].y);
    }

    drawShape();
}

function sendPanelData() {
    let bar = document.getElementsByClassName("slider");
    let cb = document.getElementById("dotLane");

    let obj = { "Pattern-Size width": parseFloat(bar[0].value),
                "Pattern-Size height": parseFloat(bar[1].value),
                "Pattern Distance width": parseInt(bar[2].value),
                "Vehicle Hood Region": parseFloat(bar[3].value),
                "Bottos' Dots Lane": cb.checked };

    let jsonStr = JSON.stringify(obj);

    downloadFile = "data.txt";
    ws.send("panel data: " + jsonStr);

}

function resetPanel() {
    let bar = document.getElementsByClassName("slider");
    for(let i = 0;i < bar.length; ++i) {
        bar[i].value = bar[i].min;
        let val = (bar[i].value - bar[i].min)/(bar[i].max - bar[i].min);
        bar[i].style.backgroundImage = '-webkit-gradient(linear, left top, right top,'
            + 'color-stop(' + val + ', #4CAF50),'
            + 'color-stop(' + val + ', #d3d3d3)' + ')';
    }
    let checkbox = document.getElementById("dotLane");
    checkbox.checked = false;
}

function pauseStreaming() {

    if(ws === null) {
        WebSocketTest(ip, port);
    }

    ws.send("Stop streaming");
    wfs.destroy();
}

function startStreaming(ip, port) {

    if(ws === null) {
        WebSocketTest(ip, port);
    }

    ws.send("Start streaming");
    if (Wfs.isSupported()) {
        wfs = null;
        let video1 = document.getElementById("video1");
        wfs = new Wfs();
        wfs.attachMedia(video1, ip, port);
    }
}

function getFileList(ip, port) {
    if(ws === null) {
        WebSocketTest(ip, port);
    }

    ws.send("hi, server");

}

function WebSocketTest(ip, port) {
    if ("WebSocket" in window) {

        if (ws === null || serverClosed === true) {
            serverClosed = false;

            ws = new WebSocket('ws://' + ip + ':' + port);
            ws.binaryData = "blob";

            ws.onopen = function() {
                //ws.send("hi, server");
            };

            ws.onmessage = function(event) {
                let rec = event.data;

                if (rec instanceof Blob) {
                    if (fileStart) {
                        myBlobBuilder.append(rec);
                    }
                } else if (typeof rec === "string") {
                    if (rec === "File starts.") {
                        fileStart = true;
                        fileEnd = false;
                        myBlobBuilder = new MyBlobBuilder();
                    } else if (rec === "File ends.") {
                        fileStart = false;
                        fileEnd = true;

                        let bb = myBlobBuilder.getBlob();
                        downloadAndShow(bb);
                        myBlobBuilder = undefined;
                    } else if (rec.substr(0, 7) === "option:") {

                        /* trim the string */
                        rec = rec.replace("option:[", "");
                        rec = rec.replace("]", "");

                        let list_DOM = document.getElementById('optionList');

                        /* remove original datalist */
                        let optNum = list_DOM.options.length;
                        for (let i = 0; i < optNum; ++i) {
                            let opt = document.getElementById('fileOption');
                            if (opt) {
                                opt.parentNode.removeChild(opt);
                            }
                        }

                        /* append new datalist */
                        let optionArray = rec.split(", ");
                        let initOptNum = list_DOM.options.length;
                        for (let i = 0; i < optionArray.length; ++i) {
                            let opt_DOM = document.createElement('option');
                            opt_DOM.value = optionArray[i];
                            opt_DOM.id = "fileOption";
                            list_DOM.appendChild(opt_DOM);
                        }
                    } else {
                        alert(rec);
                    }
                }
            };

            ws.onclose = function() {
                ws.send("exit");
                alert("connection closed.");
                serverClosed = true;
            };

        } else {
            //alert("connection exists.");
        }
    } else {
        alert("Your browser is not supported.");
    }
    return;
}

function downloadAndShow(rec) {

    /* handle the received file */
    if (rec instanceof Blob) {
        let _span = document.getElementById('rec_container');
        _span.innerHTML = "Receive file: " + downloadFile + "<br>";

        /* create download url of the file */
        let rec_url = window.URL.createObjectURL(rec);

        /* display image */
        if (downloadFile.indexOf('.jpg') !== -1 ||
            downloadFile.indexOf('.png') !== -1) {

            let img_DOM = document.getElementById("rec_img");
            if (img_DOM === null) {
                img_DOM = document.createElement("img");
                img_DOM.id = "rec_img";
                /*_span.innerHTML = "Receive image: ";*/
                _span.appendChild(img_DOM);
            }
            img_DOM.style.cssText = '';
            img_DOM.href = rec_url;
            img_DOM.src = rec_url;
        } else if (downloadFile.indexOf('.mp4') !== -1 ||
            downloadFile.indexOf('.webm') !== -1 ||
            downloadFile.indexOf('.ogv') !== -1) {

            /* hide the previous picture */
            let img_DOM = document.getElementById("rec_img");
            if (img_DOM !== null) {
                img_DOM.style.cssText = 'display: none;';
            }

            let player = document.getElementById("my-player");
            let video = document.getElementById("rec_video");
            if (video === null) {
                video = document.createElement("source");
                video.id = "rec_video";
                player.appendChild(video);
            }
            video.src = rec_url;
            video.type = "video/mp4";
            player.load();
            player.play();
        } else {
            let img_DOM = document.getElementById("rec_img");
            if (img_DOM !== null) {
                img_DOM.style.cssText = 'display: none;';
            }
        }

        /* auto download */
        let download_DOM = document.getElementById('download');
        download_DOM.href = rec_url;
        download_DOM.download = downloadFile;
        download_DOM.style.cssText = '';
        download_DOM.click();
        download_DOM.style.cssText = 'display: none;';

    }

}

function sendFile() {
    if (ws.readyState === WebSocket.OPEN) {

        if (document.getElementById('uploadFile').files.length === 0) {
            alert("Select a file to send.");
            return;
        }

        /* send filename first */
        let path = document.getElementById('uploadFile').value;
        let startIndex = (path.indexOf('\\') >= 0 ? path.lastIndexOf('\\') : path.lastIndexOf('/'));
        let filename = path.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        ws.send("file: " + filename);

        console.log(filename);

        /* send file */
        let file = document.getElementById('uploadFile').files[0];
        let reader = new FileReader();
        reader.onload = function(event) {
            rawData = event.target.result;
        };
        reader.readAsArrayBuffer(file);
        ws.send(file);
    } else {
        alert("webSocket is not ready yet.");
    }

}

function requestFile() {
    let reqPath = document.getElementById('filePath').value;
    downloadFile = reqPath.substring(reqPath.lastIndexOf('/') + 1);
    ws.send("client request: " + reqPath);
}
